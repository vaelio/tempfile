
pub mod dir {
    use rand::{thread_rng, Rng};
    use rand::distributions::Alphanumeric;
    use std::env::temp_dir;
    use std::io::Write;
    use std::fs::{File, remove_file, create_dir, remove_dir_all};


    pub struct TempDir {
        pub dirname: String,
        pub delete: bool,
    }

    impl TempDir {
        pub fn new() -> Result<TempDir, std::io::Error> {
            let rand_string: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(30)
            .map(char::from)
            .collect();
            let name: String = format!("{}/{}", temp_dir().display(), rand_string);
            let _d = create_dir(&name)?;
            let td = TempDir { dirname: name, delete: false};
            return Ok(td);

        }
    }

    
    impl Drop for TempDir {
        fn drop(&mut self) {
            if self.delete {
                remove_dir_all(&self.dirname).expect("Could not delete file");
            }
        }
    }
}

pub mod file {
    use rand::{thread_rng, Rng};
    use rand::distributions::Alphanumeric;
    use std::env::temp_dir;
    use std::io::Write;
    use std::fs::{File, remove_file};


    pub struct TempFile {
        pub filename: String,
        inner: File,
        pub delete: bool,
    }

    #[macro_export]
    macro_rules! new_tempfile {
        ($a : expr) =>  {
            self::file::TempFile::new_with_dir($a)
        };
        () => {
            self::file::TempFile::new()
        };
    }

    impl TempFile {

        pub fn new() -> Result<TempFile, std::io::Error> {
            let rand_string: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(30)
            .map(char::from)
            .collect();
            let name: String = format!("{}/{}", temp_dir().display(), rand_string);
            let f = File::create(&name)?;
            let tf = TempFile { filename: name, inner: f, delete: false};
            return Ok(tf);

        }

        pub fn new_with_dir(dir: impl AsRef<[u8]>+ std::fmt::Display) -> Result<TempFile, std::io::Error> {
            let rand_string: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(30)
            .map(char::from)
            .collect();
            let name: String = format!("{}/{}", dir, rand_string);
            let file = File::create(&name)?;
            let tf = TempFile { filename: name, inner: file, delete: false};
            return Ok(tf);

        }

    }

    
    impl Write for TempFile {
        fn write(&mut self, data: &[u8]) -> Result<usize, std::io::Error> {
            self.inner.write(data)
        }

        fn flush(&mut self) -> Result<(), std::io::Error> {
            self.inner.flush()
        }
    }

    

    impl Drop for TempFile {
        fn drop(&mut self) {
            if self.delete{
                remove_file(&self.filename).expect("Could not delete file");
            }
        }
    }

}


#[cfg(test)]
mod tests {
    use crate::file::TempFile; 
    use crate::new_tempfile;
    use crate::dir::TempDir;
    use std::io::Write;
    use std::fs::read_to_string;

    #[test]
    fn it_works_tf() {
        let mut tf = new_tempfile!()
            .expect("Could not create file");
        write!(tf, "test")
            .expect("Could not write to file");
        let content = read_to_string(&tf.filename)
            .expect("Could not read content back of the file");
        assert_eq!(content, "test", "File content is wrong");

    }

    #[test]
    fn it_works_td() {
        let td = self::TempDir::new()
            .expect("Could not create dir");
    }

    #[test]
    fn it_works_combo() {
        let td = self::TempDir::new()
            .expect("Could not create dir");
        let mut tf = new_tempfile!(&td.dirname)
            .expect("Could not create file");
        write!(tf, "test");
    }
}
